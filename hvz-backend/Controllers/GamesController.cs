﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using hvz_backend.Models.Domain;
using hvz_backend.Services;
using hvz_backend.Models.DTOs.Game;
using AutoMapper;
using System;
using System.Net.Mime;
using Microsoft.AspNetCore.Authorization;

namespace hvz_backend.Controllers
{
    [Route("api/game")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class GamesController : ControllerBase
    {
        private readonly IGameService _gameService;
        private readonly IMapper _mapper;

        public GamesController(IGameService gameService, IMapper mapper)
        {
            _gameService = gameService;
            _mapper = mapper;
        }

        /// <summary>
        /// Returns a list of all games.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GameReadDTO>>> GetGames()
        {
            return _mapper.Map<List<GameReadDTO>>(await _gameService.GetGamesAsync());
        }

        /// <summary>
        /// Gets a specific game by id.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{gameId}")]
        public async Task<ActionResult<GameReadDTO>> GetGame(int gameId)
        {
            Game game = await _gameService.GetSpecificGameAsync(gameId);

            if (game == null)
            {
                return NotFound();
            }

            return _mapper.Map<GameReadDTO>(game);
        }

        /// <summary>
        /// Post New Game, admin only.
        /// </summary>
        /// <param name="game">Game Create DTO</param>
        /// <returns>Information of the game</returns>
        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<ActionResult<GameReadDTO>> PostGame(GameCreateDTO game)
        {
            if(game.NwLat == 0.0 && game.NwLon == 0.0 && game.SeLat == 0.0 && game.SeLon == 0.0)
            {
                return NotFound();
            }
            Game domainGame = _mapper.Map<Game>(game);
            domainGame.DateOfCreation = DateTime.Now;
            domainGame.DateOfCompletion = null;
            return _mapper.Map<GameReadDTO>(await _gameService.AddNewGameAsync(domainGame));
        }

        /// <summary>
        /// Edit an existing game.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="updatedGame">Updated game</param>
        /// <returns></returns>
        [Authorize(Roles = "admin")]
        [HttpPut("{gameId}")]
        public async Task<IActionResult> PutGame(int gameId, GameEditDTO updatedGame)
        {
            if (gameId != updatedGame.GameId)
            {
                return BadRequest();
            }

            if (!_gameService.GameExists(gameId))
            {
                return NotFound();
            }
            Game domainGame = _mapper.Map<Game>(updatedGame);
            if (updatedGame.GameState == "Completed")
            {
                domainGame.DateOfCompletion = DateTime.Now;
            }           
            await _gameService.UpdateGameAsync(domainGame);

            return NoContent();
        }

        /// <summary>
        /// Delete game by id.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <returns>Not found/Bad Request/NoContent</returns>
        [Authorize(Roles = "admin")]
        [HttpDelete("{gameId}")] //@Todo Must be cascading
        public async Task<IActionResult> DeleteGame(int gameId)
        {
            if (!_gameService.GameExists(gameId))
            {
                return NotFound();
            }

            try
            {
                await _gameService.DeleteGameAsync(gameId);
            }
            catch
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
