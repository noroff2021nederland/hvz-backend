﻿using AutoMapper;
using hvz_backend.Models.Domain;
using hvz_backend.Models.DTOs.Kill;
using hvz_backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;

namespace hvz_backend.Controllers
{
    [Route("api/game")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class KillController : ControllerBase
    {
        private readonly IGameService _gameService;
        private readonly IKillService _killService;
        private readonly IPlayerService _playerService;
        private readonly IMapper _mapper;

        public KillController(IGameService gameService, IKillService killService, IPlayerService playerService, IMapper mapper)
        {
            _gameService = gameService;
            _killService = killService;
            _playerService = playerService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all kills in a game.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <returns>A list with details of all kills</returns>
        [Authorize]
        [HttpGet("{gameId}/kill")]
        public async Task<ActionResult<IEnumerable<KillReadDTO>>> GetKills(int gameId)
        {
            return _mapper.Map<List<KillReadDTO>>(await _killService.GetKillsAsync(gameId));
        }

        /// <summary>
        /// Get a specific kill in a game.
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="killId"></param>
        /// <returns>Information on the kill</returns>
        [Authorize]
        [HttpGet("{gameId}/kill/{killId}")]
        public async Task<ActionResult<KillReadDTO>> GetSpecificKill(int gameId, int killId)
        {
            if (!_gameService.GameExists(gameId) || !_killService.KillExists(killId))
            {
                return NoContent();
            }

            Kill domainKill = await _killService.GetSpecificKillAsync(killId);

            if (domainKill.GameId != gameId)
            {
                return BadRequest();
            }

            return _mapper.Map<KillReadDTO>(domainKill);
        }

        /// <summary>
        /// Adds a new kill, within a game with a bitecode.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="newKill">New kill</param>
        /// <returns>New created kill</returns>
        [Authorize]
        [HttpPost("{gameId}/kill")]//@Todo Admin only is some cases, does this need to flip the isHuman property?
        public async Task<ActionResult> PostKill(int gameId, KillCreateDTO newKill)
        {
            if (!_gameService.GameExists(gameId))
            {
                return NotFound();
            }
            if (newKill.Lon == 0.0 && newKill.Lat == 0.0)
            {
                return NotFound();
            }
            if (!await _gameService.ValidLocationAsync(gameId, newKill.Lon, newKill.Lat))
            {
                return BadRequest();
            }
            Player killer = await _playerService.GetSpecificPlayerAsync(newKill.KillerId);
            if (newKill.BiteCode != await _playerService.GetBiteCodeFromVictimAsync(newKill.VictimId) || killer.IsHuman)
            {
                return BadRequest();
            }

            //Turns the victim into a zombie
            Player victim = await _playerService.GetSpecificPlayerAsync(newKill.VictimId);
            victim.IsHuman = false;
            await _playerService.UpdatePlayerAsync(victim);

            Kill domainKill = _mapper.Map<Kill>(newKill);
            domainKill.TimeOfDeath = System.DateTime.Now;

            domainKill = await _killService.AddKillAsync(domainKill);
            KillReadDTO killReadDTO = _mapper.Map<KillReadDTO>(domainKill);

            return CreatedAtAction("GetSpecificKill", new { gameId = gameId, killId = killReadDTO.KillId }, killReadDTO);
        }

        /// <summary>
        /// Deletes a kill of a game .
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="killId">Kill Id</param>
        /// <returns></returns>
        [Authorize(Roles = "admin")]
        [HttpDelete("{gameId}/kill/{killId}")]
        public async Task<IActionResult> DeleteKill(int gameId, int killId)
        {
            if (!_gameService.GameExists(gameId) || !_killService.KillExists(killId))
            {
                return NotFound();
            }

            try
            {
                Kill deletedKill = await _killService.DeleteKillAsync(killId);

                //nullify the zombiestatus of the victim in case the kill is deleted
                Player playerToBeEdited = await _playerService.GetSpecificPlayerAsync(deletedKill.VictimId);
                playerToBeEdited.IsHuman = true;
                await _playerService.UpdatePlayerAsync(playerToBeEdited);
            }
            catch
            {
                return BadRequest();
            }
            return NoContent();
        }

        /// <summary>
        /// Put request for kill, only the admin or the killer might update this. 
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="killId">Kill Id</param>
        /// <param name="updatedKill">Updated kill</param>
        /// <returns></returns>
        [Authorize(Roles = "admin")]
        [HttpPut("{gameId}/kill/{killId}")] //@Todo Killer only
        public async Task<IActionResult> PutKill(int gameId, int killId, KillEditDTO updatedKill)
        {
            if (updatedKill.KillId != killId || updatedKill.GameId != gameId)
            {
                return BadRequest();
            }

            if (!_gameService.GameExists(gameId) || !_killService.KillExists(killId))
            {
                return NotFound();
            }

            Kill domainKill = _mapper.Map<Kill>(updatedKill);
            await _killService.UpdateKillAsync(domainKill);

            return NoContent();
        }
    }
}
