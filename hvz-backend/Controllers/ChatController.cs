﻿using AutoMapper;
using hvz_backend.Models.Domain;
using hvz_backend.Models.DTOs.Chat;
using hvz_backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;

namespace hvz_backend.Controllers
{
    [Route("api/game")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class ChatController : ControllerBase
    {
        private readonly IGameService _gameService;
        private readonly IChatService _chatService;
        private readonly IPlayerService _playerService;
        private readonly IMapper _mapper;

        public ChatController(IGameService gameService, IChatService chatService, IPlayerService playerService, IMapper mapper)
        {
            _gameService = gameService;
            _chatService = chatService;
            _playerService = playerService;
            _mapper = mapper;
            
        }
        /// <summary>
        /// Request all global messages for zombie or human player.
        /// </summary>
        /// <param name="gameId">Game Id</param> 
        /// <param name="playerId">Insert playerid to specify whether a player is zombie or human</param>
        /// <returns>List of Messages</returns>
        [Authorize]
        [HttpGet("{gameId}/chat")] //@Todo check if player is actually human/zombie
        public async Task<ActionResult<List<ChatReadDTO>>> GetAllAppropriateChatMessages(int gameId, int playerId)
        {
            if (!_gameService.GameExists(gameId) || !_playerService.PlayerExists(playerId))
            {
                return NotFound();
            }
            Player player = await _playerService.GetSpecificPlayerAsync(playerId);
            return _mapper.Map<List<ChatReadDTO>>(await _chatService.GetChatMessagesAsync(player.IsHuman, !player.IsHuman));
        }

        /// <summary>
        /// Posts a chat message.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="newMessage"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("{gameId}/chat")] //@Todo change CreateAtAction to single message
        public async Task<ActionResult<ChatReadDTO>> PostChatMessage(int gameId, ChatCreateDTO newMessage)
        {
            if (!_gameService.GameExists(gameId))
            {
                return NotFound();
            }

            if (newMessage.GameId != gameId)
            {
                return BadRequest();
            }

            Chat domainChat = _mapper.Map<Chat>(newMessage);
            domainChat.Time = System.DateTime.Now;

            domainChat = await _chatService.AddChatMessageAsync(domainChat);
            ChatReadDTO chatReadDTO = _mapper.Map<ChatReadDTO>(domainChat);

            return CreatedAtAction("GetAllAppropriateChatMessages", new { id = chatReadDTO.ChatId }, chatReadDTO);
        }
    }
}
