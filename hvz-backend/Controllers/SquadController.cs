﻿using AutoMapper;
using hvz_backend.Models.Domain;
using hvz_backend.Models.DTOs.Chat;
using hvz_backend.Models.DTOs.Player;
using hvz_backend.Models.DTOs.Squad;
using hvz_backend.Models.DTOs.SquadCheckin;
using hvz_backend.Models.DTOs.SquadMember;
using hvz_backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;

namespace hvz_backend.Controllers
{
    [Route("api/game")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class SquadController : ControllerBase
    {
        private readonly IGameService _gameService;
        private readonly ISquadService _squadService;
        private readonly IPlayerService _playerService;
        private readonly IMapper _mapper;

        public SquadController(IGameService gameService, ISquadService squadService, IPlayerService playerService, IMapper mapper)
        {
            _gameService = gameService;
            _squadService = squadService;
            _playerService = playerService;
            _mapper = mapper;
        }

        /// <summary>
        /// Returns a list of all squads.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{gameId}/squad")]
        public async Task<ActionResult<IEnumerable<SquadReadDTO>>> GetSquads(int gameId)
        {
            return _mapper.Map<List<SquadReadDTO>>(await _squadService.GetSquadsAsync(gameId));
        }

        /// <summary>
        /// Returns a specific squad.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="squadId">Squad Id</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{gameId}/squad/{squadId}")]
        public async Task<ActionResult<SquadReadDTO>> GetSpecificSquad(int gameId, int squadId)
        {
            if (!_squadService.SquadExists(gameId, squadId))
            {
                return NotFound();
            }

            Squad squad = await _squadService.GetSquadByIdAsync(gameId, squadId);

            if (squad == null)
            {
                return NotFound();
            }

            return _mapper.Map<SquadReadDTO>(squad);
        }

        /// <summary>
        /// Adds a new squad in a current game.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="newSquad">New squad</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("{gameId}/squad")]
        public async Task<ActionResult<SquadReadDTO>> PostSquad(int gameId, SquadCreateDTO newSquad)
        {
            if (!_gameService.GameExists(gameId))
            {
                return NotFound();
            }

            if (newSquad.GameId != gameId)
            {
                return BadRequest();
            }

            Squad domainSquad = await _squadService.AddSquadAsync(_mapper.Map<Squad>(newSquad));
            SquadReadDTO squadReadDTO = _mapper.Map<SquadReadDTO>(domainSquad);

            //@TODO: Rank the player who registers the squad

            return CreatedAtAction("GetSquads", new { id = squadReadDTO.SquadId }, squadReadDTO);
        }

        /// <summary>
        /// Adds a member to a squad with the initial rank of solder.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="squadId">Squad Id</param>
        /// <param name="player">Player</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("{gameId}/squad/{squadId}/join")]
        public async Task<ActionResult<SquadMemberReadDTO>> JoinSquad(int gameId, int squadId, PlayerReadDTO player)
        {
            if (!_squadService.SquadExists(gameId, squadId))
            {
                return NotFound();
            }

            if (!_playerService.PlayerExistsInGame(gameId, player.PlayerId))
            {
                return NotFound();
            }

            var member = new SquadMemberCreateDTO()
            {
                GameId = gameId,
                PlayerId = player.PlayerId,
                SquadId = squadId,
                Rank = "Soldier"
            };

            SquadMember domainSquadMember = await _squadService.JoinSquad(_mapper.Map<SquadMember>(member));
            SquadMemberReadDTO squadReadDTO = _mapper.Map<SquadMemberReadDTO>(domainSquadMember);

            return CreatedAtAction("GetSquads", new { id = squadReadDTO.SquadMemberId }, squadReadDTO);
        }

        /// <summary>
        /// Updates a squad.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="squadId">Squad Id</param>
        /// <param name="updatedSquad">Updated Squad</param>
        /// <returns></returns>
        [Authorize]
        [HttpPut("{gameId}/squad/{squadId}")]
        public async Task<IActionResult> PutSquad(int gameId, int squadId, SquadEditDTO updatedSquad)
        {
            if (gameId != updatedSquad.GameId)
            {
                return BadRequest();
            }

            if (!_squadService.SquadExists(gameId, squadId))
            {
                return NotFound();
            }

            Squad domainSquad = _mapper.Map<Squad>(updatedSquad);
            await _squadService.UpdateSquadAsync(domainSquad);

            return NoContent();
        }

        /// <summary>
        /// Deletes a squad by id.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="squadId">Squad Id</param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete("{gameId}/squad/{squadId}")]
        public async Task<IActionResult> DeleteSquad(int gameId, int squadId)
        {
            if (!_squadService.SquadExists(gameId, squadId))
            {
                return NotFound();
            }

            try
            {
                await _squadService.DeleteSquadAsync(squadId);
            }
            catch
            {
                return BadRequest();
            }

            return NoContent();
        }

        /// <summary>
        /// Returns list of chat messages.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="squadId">Squad id</param>
        /// <param name="playerId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{gameId}/squad/{squadId}/chat")]
        public async Task<ActionResult<IEnumerable<ChatReadDTO>>> GetChatMessages(int gameId, int squadId, int playerId)
        {
            if (!_squadService.SquadExists(gameId, squadId))
            {
                return NotFound();
            }

            SquadMember squadmember = await _squadService.GetSquadMemberByPlayerIdAsync(playerId);
            if(squadmember == null || squadmember.SquadId != squadId)
            {
                return BadRequest();
            }

            //@TODO: get user from auth headers (for only displaying appropriate messages)
            var messages = await _squadService.GetChatMessagesAsync(gameId, squadId);

            if (messages == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<ChatReadDTO>>(messages);
        }

        /// <summary>
        /// Adds a new chat message.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="squadId">Squad Id</param>
        /// <param name="newMessage">New Message</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("{gameId}/squad/{squadId}/chat")]
        public async Task<ActionResult<ChatReadDTO>> PostChatMessage(int gameId, int squadId, ChatCreateDTO newMessage)
        {            
            if (!_squadService.SquadExists(gameId, squadId))
            {
                return NotFound();
            }

            if (newMessage.GameId != gameId || newMessage.SquadId != squadId)
            {
                return BadRequest();
            }

            if(newMessage.IsHumanGlobal || newMessage.IsZombieGlobal)
            {
                return NotFound();
            }

            SquadMember squadmember = await _squadService.GetSquadMemberByPlayerIdAsync(newMessage.PlayerId);
            if (squadmember == null || squadmember.SquadId != squadId)
            {
                return BadRequest();
            }

            Chat domainChat = await _squadService.AddChatMessage(_mapper.Map<Chat>(newMessage));
            domainChat.Time = System.DateTime.Now;
            ChatReadDTO chatReadDTO = _mapper.Map<ChatReadDTO>(domainChat);

            return CreatedAtAction("PostChatMessage", new { id = chatReadDTO.ChatId }, chatReadDTO);
        }

        /// <summary>
        /// Returns a list of squad checkins.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="squadId">Squad Id</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{gameId}/squad/{squadId}/check-in")]
        public async Task<ActionResult<IEnumerable<SquadCheckinReadDTO>>> GetCheckins(int gameId, int squadId)
        {
            if (!_squadService.SquadExists(gameId, squadId))
            {
                return NotFound();
            }

            //@TODO: get user from auth headers (for only displaying appropriate messages)
            var checkins = await _squadService.GetCheckinsAsync(gameId, squadId);

            if (checkins == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<SquadCheckinReadDTO>>(checkins);
        }

        /// <summary>
        /// Add a new check-in.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="squadId">Squad Id</param>
        /// <param name="newCheckin">New check-in</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("{gameId}/squad/{squadId}/check-in")]
        public async Task<ActionResult<SquadCheckinReadDTO>> PostCheckin(int gameId, int squadId, SquadCheckinCreateDTO newCheckin)
        {
            if (!_squadService.SquadExists(gameId, squadId))
            {
                return NotFound();
            }
            if(newCheckin.Lon == 0.0 && newCheckin.Lat == 0.0)
            {
                return NotFound();
            }
            if (!await _gameService.ValidLocationAsync(gameId, newCheckin.Lon, newCheckin.Lat))
            {
                return BadRequest();
            }
            if (newCheckin.GameId != gameId || newCheckin.SquadId != squadId)
            {
                return BadRequest();
            }

            SquadCheckin domainChat = await _squadService.AddCheckin(_mapper.Map<SquadCheckin>(newCheckin));
            SquadCheckinReadDTO squadCheckinReadDTO = _mapper.Map<SquadCheckinReadDTO>(domainChat);

            return CreatedAtAction("PostCheckin", new { id = squadCheckinReadDTO.SquadCheckinId }, squadCheckinReadDTO);
        }
    }
}
