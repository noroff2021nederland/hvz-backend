﻿using AutoMapper;
using hvz_backend.Models.Domain;
using hvz_backend.Models.DTOs.Game;
using hvz_backend.Models.DTOs.User;
using hvz_backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace hvz_backend.Controllers
{
    [Route("api/user")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IGameService _gameService;
        private readonly IPlayerService _playerService;
        private readonly IMapper _mapper;

        public UserController(IUserService userService, IGameService gameService, IPlayerService playerService, IMapper mapper)
        {
            _userService = userService;
            _playerService = playerService;
            _gameService = gameService;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets User with a KeyCloackId
        /// </summary>
        /// <param name="keyCloakId">KeyCloak Id</param>
        /// <returns>User with a specific KeyCloakId</returns>
        [HttpGet("{keyCloakId}")]
        public async Task<ActionResult<UserReadDTO>> GetSpecificUser(string keyCloakId)
        {
            return _mapper.Map<UserReadDTO>(await _userService.GetSpecificUserByKeyCloakIdAsync(keyCloakId));
        }

        [HttpPost]
        public async Task<ActionResult<UserReadDTO>> PostNewUser(UserCreateDTO user)
        {
            User domainUser = _mapper.Map<User>(user);
            return _mapper.Map<UserReadDTO>(await _userService.AddNewUserAsync(domainUser));
        }
    }
}

