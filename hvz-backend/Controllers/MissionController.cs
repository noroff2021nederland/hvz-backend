﻿using AutoMapper;
using hvz_backend.Models.Domain;
using hvz_backend.Models.DTOs.Mission;
using hvz_backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;

namespace hvz_backend.Controllers
{
    [Route("api/game")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class MissionController : ControllerBase
    {
        private readonly IGameService _gameService;
        private readonly IMissionService _missionService;
        private readonly IMapper _mapper;

        public MissionController(IGameService gameService, IMissionService missionService, IMapper mapper)
        {
            _gameService = gameService;
            _missionService = missionService;
            _mapper = mapper;
        }

        /// <summary>
        /// Returns a list of missions.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{gameId}/mission")]
        public async Task<ActionResult<IEnumerable<MissionReadDTO>>> GetMissions(int gameId)
        {
            return _mapper.Map<List<MissionReadDTO>>(await _missionService.GetMissionsAsync(gameId));
        }

        /// <summary>
        /// Return a specific mission by id.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="missionId">Mission Id</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{gameId}/mission/{missionId}")]
        public async Task<ActionResult<MissionReadDTO>> GetSpecificMission(int gameId, int missionId)
        {
            if (!_missionService.MissionExists(gameId, missionId))
            {
                return NotFound();
            }

            Mission mission = await _missionService.GetMissionByIdAsync(gameId, missionId);

            if (mission == null)
            {
                return NotFound();
            }

            return _mapper.Map<MissionReadDTO>(mission);
        }

        /// <summary>
        /// Returns all missions across all games.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("missions")]
        public async Task<ActionResult<IEnumerable<MissionReadDTO>>> GetAllMissions()
        {
            return _mapper.Map<List<MissionReadDTO>>(await _missionService.GetAllMissionsAsync());
        }

        /// <summary>
        /// Create a new mission.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="newMission">New mission</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("{gameId}/mission")]
        public async Task<ActionResult<MissionReadDTO>> PostMission(int gameId, MissionCreateDTO newMission)
        {
            if (!_gameService.GameExists(gameId))
            {
                return NotFound();
            }

            if (newMission.GameId != gameId)
            {
                return BadRequest();
            }
            if (newMission.Lon == 0.0 && newMission.Lat == 0.0)
            {
                return NotFound();
            }
            if (!await _gameService.ValidLocationAsync(gameId, newMission.Lon, newMission.Lat))
            {
                return BadRequest();
            }

            Mission domainMission = await _missionService.AddMissionAsync(_mapper.Map<Mission>(newMission));
            MissionReadDTO missionReadDTO = _mapper.Map<MissionReadDTO>(domainMission);

            //@TODO: Rank the player who registers the squad

            return CreatedAtAction("GetMissions", new { id = missionReadDTO.MissionId }, missionReadDTO);
        }

        /// <summary>
        /// Update existing mission.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="missionId">Mission Id</param>
        /// <param name="updatedMission">Updated mission</param>
        /// <returns></returns>
        [Authorize]
        [HttpPut("{gameId}/mission/{missionId}")]
        public async Task<IActionResult> PutSquad(int gameId, int missionId, MissionEditDTO updatedMission)
        {
            if (gameId != updatedMission.GameId || missionId != updatedMission.MissionId)
            {
                return BadRequest();
            }

            if (!_missionService.MissionExists(gameId, missionId))
            {
                return NotFound();
            }

            Mission domainMission = _mapper.Map<Mission>(updatedMission);
            await _missionService.UpdateMissionAsync(domainMission);

            return NoContent();
        }

        /// <summary>
        /// Deletes mission by game and mission id.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="missionId">Mission Id</param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete("{gameId}/mission/{missionId}")]
        public async Task<IActionResult> DeleteSquad(int gameId, int missionId)
        {
            if (!_missionService.MissionExists(gameId, missionId))
            {
                return NotFound();
            }

            try
            {
                await _missionService.DeleteMissionAsync(missionId);
            }
            catch
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
