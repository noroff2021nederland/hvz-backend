﻿using AutoMapper;
using hvz_backend.Models.Domain;
using hvz_backend.Models.DTOs.Player;
using hvz_backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;

namespace hvz_backend.Controllers
{
    [Route("api/game")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class PlayerController : ControllerBase
    {
        private readonly IPlayerService _playerService;
        private readonly IGameService _gameService;
        private readonly IMapper _mapper;

        public PlayerController(IPlayerService playerService, IGameService gameService, IMapper mapper)
        {
            _playerService = playerService;
            _gameService = gameService;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all the players in a specific game.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <returns>List of players with their information</returns>
        [Authorize]
        [HttpGet("{gameId}/player")] //@Todo Possible necessary duplicate for admin, to see specific flags like: IsPatientZero, maybe make a seperated DTO to display this information
        public async Task<ActionResult<IEnumerable<PlayerReadDTO>>> GetAllPlayers(int gameId)
        {
            if (!_gameService.GameExists(gameId))
            {
                return NotFound();
            }

            return _mapper.Map<List<PlayerReadDTO>>(await _playerService.GetPlayersAsync(gameId));
        }

        /// <summary>
        /// Gets the number of players in a specific game.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <returns></returns>
        [HttpGet("{gameId}/player/count")] //@Todo Possible necessary duplicate for admin, to see specific flags like: IsPatientZero, maybe make a seperated DTO to display this information
        public async Task<ActionResult<int>> GetPlayerCount(int gameId)
        {
            if (!_gameService.GameExists(gameId))
            {
                return NotFound();
            }

            return await _playerService.CountPlayersAsync(gameId);
        }

        /// <summary>
        /// Get a specific player from a specific game.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="playerId">Player Id</param>
        /// <returns>Player information</returns>
        [Authorize]
        [HttpGet("{gameId}/player/{playerId}")] //@Todo Possible necessary duplicate for admin, to see specific flags like: IsPatientZero, maybe make a seperated DTO to display this information
        public async Task<ActionResult<PlayerReadDTO>> GetSpecificPlayerByID(int gameId, int playerId)
        {
            Player domainPlayer = _mapper.Map<Player>(await _playerService.GetSpecificPlayerAsync(playerId));

            if (!_gameService.GameExists(gameId) || !_playerService.PlayerExists(playerId) || domainPlayer.GameId != gameId)
            {
                return NotFound();
            }

            return _mapper.Map<PlayerReadDTO>(domainPlayer);
        }

        /// <summary>
        /// Creates a new empty player profile.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="newPlayer">New player</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("{gameId}/player")]
        public async Task<ActionResult<PlayerReadDTO>> PostNewPlayerToGame(int gameId, PlayerCreateDTO newPlayer)
        {
            if (!_gameService.GameExists(gameId) || !_playerService.UserExists(newPlayer.UserId))
            {
                return NotFound();
            }

            Player domainPlayer = await _playerService.AddPlayerAsync(_mapper.Map<Player>(newPlayer));
            PlayerReadDTO playerReadDTO = _mapper.Map<PlayerReadDTO>(domainPlayer);

            return CreatedAtAction("GetSpecificPlayerByID", new { gameId = gameId, playerId = playerReadDTO.PlayerId }, playerReadDTO);
        }

        /// <summary>
        /// Edits a specific player in a specific game.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="playerId">Player Id</param>
        /// <param name="updatedPlayer">Updated player</param>
        /// <returns>No content</returns>
        [Authorize(Roles = "admin")]
        [HttpPut("{gameId}/player/{playerId}")]
        public async Task<IActionResult> PutPlayer(int gameId, int playerId, PlayerEditDTO updatedPlayer)
        {
            if (!_gameService.GameExists(gameId) || !_playerService.PlayerExists(playerId))
            {
                return NotFound();
            }
            if (updatedPlayer.PlayerId != playerId)
            {
                return BadRequest();
            }

            Player domainPlayer = _mapper.Map<Player>(updatedPlayer);

            await _playerService.UpdatePlayerAsync(domainPlayer);

            return NoContent();
        }

        /// <summary>
        /// Deletes a player in a specific game, can only delete the player in the same game as the user requested.
        /// </summary>
        /// <param name="gameId">Game Id</param>
        /// <param name="playerId">Player Id</param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete("{gameId}/player/{playerId}")]
        public async Task<IActionResult> DeletePlayer(int gameId, int playerId)
        {
            if (!_gameService.GameExists(gameId) || !_playerService.PlayerExists(playerId))
            {
                return NotFound();
            }

            Player player = await _playerService.GetSpecificPlayerAsync(playerId);

            if (player.GameId != gameId)
            {
                return Unauthorized();
            }

            try
            {
                await _playerService.DeletePlayerAsync(playerId);
            }
            catch
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
