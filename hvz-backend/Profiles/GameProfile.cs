﻿using AutoMapper;
using hvz_backend.Models.Domain;
using hvz_backend.Models.DTOs.Game;

namespace hvz_backend.Profiles
{
    public class GameProfile : Profile
    {
        public GameProfile()
        {
            //Game <-> GameCreateDTO
            CreateMap<Game, GameCreateDTO>()
                .ReverseMap();
            //Game <-> GameEditDTO
            CreateMap<Game, GameEditDTO>()
                .ReverseMap();
            //Game <-> GameReadDTO
            CreateMap<Game, GameReadDTO>()
                .ReverseMap();
        }
    }
}
