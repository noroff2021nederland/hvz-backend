﻿using AutoMapper;
using hvz_backend.Models.Domain;
using hvz_backend.Models.DTOs.Kill;

namespace hvz_backend.Profiles
{
    public class KillProfile : Profile
    {
        public KillProfile()
        {
            //Kill <-> KillCreateDTO
            CreateMap<Kill, KillCreateDTO>()
                .ReverseMap();
            //Kill <-> KillEditDTO
            CreateMap<Kill, KillEditDTO>()
                .ReverseMap();
            //Kill <-> KillReadDTO
            CreateMap<Kill, KillReadDTO>()
                .ReverseMap();
        }
    }
}
