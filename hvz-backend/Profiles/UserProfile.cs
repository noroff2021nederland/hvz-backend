﻿using AutoMapper;
using hvz_backend.Models.Domain;
using hvz_backend.Models.DTOs.User;

namespace hvz_backend.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            //User <-> UserCreatDTO
            CreateMap<User, UserCreateDTO>()
                .ReverseMap();
            //User <-> UserEditDTO
            CreateMap<User, UserEditDTO>()
                .ReverseMap();
            //User <-> UserReadDTO
            CreateMap<User, UserReadDTO>()
                .ReverseMap();
        }
    }
}
