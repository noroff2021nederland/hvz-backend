﻿using AutoMapper;
using hvz_backend.Models.Domain;
using hvz_backend.Models.DTOs.SquadMember;

namespace hvz_backend.Profiles
{
    public class SquadMemberProfile : Profile
    {
        public SquadMemberProfile()
        {
            //SquadMember <-> SquadMemberCreateDTO
            CreateMap<SquadMember, SquadMemberCreateDTO>()
                .ReverseMap();
            //SquadMember <-> SquadMemberEditDTO
            CreateMap<SquadMember, SquadMemberEditDTO>()
                .ReverseMap();
            //SquadMember <-> SquadMemberReadDTO
            CreateMap<SquadMember, SquadMemberReadDTO>()
                .ReverseMap();
        }
    }
}
