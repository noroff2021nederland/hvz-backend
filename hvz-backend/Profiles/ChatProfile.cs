﻿using AutoMapper;
using hvz_backend.Models.Domain;
using hvz_backend.Models.DTOs.Chat;

namespace hvz_backend.Profiles
{
    public class ChatProfile : Profile
    {
        public ChatProfile()
        {
            // Chat<->ChatCreateDTO
            CreateMap<Chat, ChatCreateDTO>()
                .ReverseMap();
            // Chat<->ChatEditDTO
            CreateMap<Chat, ChatEditDTO>()
                .ReverseMap();
            // Chat<->ChatReadDTO
            CreateMap<Chat, ChatReadDTO>()
                .ReverseMap();
        }
    }
}
