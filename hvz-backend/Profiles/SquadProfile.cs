﻿using AutoMapper;
using hvz_backend.Models.Domain;
using hvz_backend.Models.DTOs.Squad;

namespace hvz_backend.Profiles
{
    public class SquadProfile : Profile
    {
        public SquadProfile()
        {
            //Squad <-> SquadCreateDTO
            CreateMap<Squad, SquadCreateDTO>()
                .ReverseMap();
            //Squad <-> SquadEditDTO
            CreateMap<Squad, SquadEditDTO>()
                .ReverseMap();
            //Squad <-> SquadReadDTO
            CreateMap<Squad, SquadReadDTO>()
                .ReverseMap();
        }
    }
}
