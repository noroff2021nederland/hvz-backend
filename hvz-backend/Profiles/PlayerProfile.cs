﻿using AutoMapper;
using hvz_backend.Models.Domain;
using hvz_backend.Models.DTOs.Player;

namespace hvz_backend.Profiles
{
    public class PlayerProfile : Profile
    {
        public PlayerProfile()
        {
            //Player <-> PlayerCreateDTO
            CreateMap<Player, PlayerCreateDTO>()
                .ReverseMap();
            //Player <-> PlayerEditDTO
            CreateMap<Player, PlayerEditDTO>()
                .ReverseMap();
            //Player <-> PlayerReadDTO
            CreateMap<Player, PlayerReadDTO>()
                .ReverseMap();
        }
    }
}
