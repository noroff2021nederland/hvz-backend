﻿using AutoMapper;
using hvz_backend.Models.Domain;
using hvz_backend.Models.DTOs.SquadCheckin;

namespace hvz_backend.Profiles
{
    public class SquadCheckinProfile : Profile
    {
        public SquadCheckinProfile()
        {
            //SquadCheckin <-> SquadCheckinCreateDTO
            CreateMap<SquadCheckin, SquadCheckinCreateDTO>()
                .ReverseMap();
            //SquadCheckin <-> SquadCheckinReadDTO
            CreateMap<SquadCheckin, SquadCheckinReadDTO>()
                .ReverseMap();
        }
    }
}
