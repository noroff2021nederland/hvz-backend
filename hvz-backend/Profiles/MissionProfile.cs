﻿using AutoMapper;
using hvz_backend.Models.Domain;
using hvz_backend.Models.DTOs.Mission;

namespace hvz_backend.Profiles
{
    public class MissionProfile : Profile
    {
        public MissionProfile()
        {
            //Mission <-> MissionCreateDTO
            CreateMap<Mission, MissionCreateDTO>()
                .ReverseMap();
            //Mission <-> MissionEditDTO
            CreateMap<Mission, MissionEditDTO>()
                .ReverseMap();
            //Mission <-> MissionReadDTO
            CreateMap<Mission, MissionReadDTO>()
                .ReverseMap();
        }
    }
}
