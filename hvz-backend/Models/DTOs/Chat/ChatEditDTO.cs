﻿namespace hvz_backend.Models.DTOs.Chat
{
    public class ChatEditDTO
    {
        public int ChatId { get; set; }
        public string Message { get; set; }
    }
}
