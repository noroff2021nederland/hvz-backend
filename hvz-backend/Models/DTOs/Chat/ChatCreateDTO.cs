﻿namespace hvz_backend.Models.DTOs.Chat
{
    public class ChatCreateDTO
    {
        public string Message { get; set; }
        public bool IsHumanGlobal { get; set; }
        public bool IsZombieGlobal { get; set; }
        public int GameId { get; set; }
        public int PlayerId { get; set; }
        public int? SquadId { get; set; }
    }
}
