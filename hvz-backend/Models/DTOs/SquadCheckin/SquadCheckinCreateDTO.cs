﻿using System;

namespace hvz_backend.Models.DTOs.SquadCheckin
{
    public class SquadCheckinCreateDTO
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
        public int GameId { get; set; }
        public int SquadId { get; set; }
        public int SquadMemberId { get; set; }
    }
}
