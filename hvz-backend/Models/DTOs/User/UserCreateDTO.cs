﻿namespace hvz_backend.Models.DTOs.User
{
    public class UserCreateDTO
    {
        public string KeyCloakId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
