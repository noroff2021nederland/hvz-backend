﻿namespace hvz_backend.Models.DTOs.User
{
    public class UserReadDTO
    {
        public int UserId { get; set; }
        public string KeyCloakId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
