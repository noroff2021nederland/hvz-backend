﻿using System;

namespace hvz_backend.Models.DTOs.Mission
{
    public class MissionReadDTO
    {
        public int MissionId { get; set; }
        public string Name { get; set; }
        public bool IsHumanVisible { get; set; }
        public bool IsZombieVisible { get; set; }
        public string Description { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int GameId { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
    }
}
