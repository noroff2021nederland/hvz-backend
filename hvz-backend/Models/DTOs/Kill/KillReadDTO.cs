﻿using System;

namespace hvz_backend.Models.DTOs.Kill
{
    public class KillReadDTO
    {
        public int KillId { get; set; }
        public DateTime TimeOfDeath { get; set; }
        public string Story { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public int GameId { get; set; }
        public int KillerId { get; set; }
        public int VictimId { get; set; }
    }
}
