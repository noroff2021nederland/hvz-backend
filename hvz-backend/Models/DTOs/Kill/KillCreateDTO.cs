﻿using System;

namespace hvz_backend.Models.DTOs.Kill
{
    public class KillCreateDTO
    { 
        public string Story { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public int GameId { get; set; }
        public int KillerId { get; set; }
        public int VictimId { get; set; }
        public int BiteCode { get; set; }

    }
}
