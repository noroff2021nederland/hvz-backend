﻿namespace hvz_backend.Models.DTOs.Player
{
    public class PlayerCreateDTO
    {
        public int GameId { get; set; }
        public string PlayerName { get; set; }
        public int UserId { get; set; }
    }
}
