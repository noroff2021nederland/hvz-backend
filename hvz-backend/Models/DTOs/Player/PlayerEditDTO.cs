﻿namespace hvz_backend.Models.DTOs.Player
{
    public class PlayerEditDTO
    {
        public int PlayerId { get; set; }
        public string PlayerName { get; set; }
        public bool IsHuman { get; set; }
        public bool IsPatientZero { get; set; }
        public int BiteCode { get; set; }
        public int UserId { get; set; }
        public int GameId { get; set; }
    }
}
