﻿namespace hvz_backend.Models.DTOs.SquadMember
{
    public class SquadMemberReadDTO
    {
        public int SquadMemberId { get; set; }
        public string Rank { get; set; }
    }
}
