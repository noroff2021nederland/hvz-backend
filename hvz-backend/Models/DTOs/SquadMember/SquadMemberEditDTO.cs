﻿namespace hvz_backend.Models.DTOs.SquadMember
{
    public class SquadMemberEditDTO
    {
        public int SquadMemberId { get; set; }
        public string Rank { get; set; }
    }
}
