﻿namespace hvz_backend.Models.DTOs.SquadMember
{
    public class SquadMemberCreateDTO
    {
        public string Rank { get; set; }
        public int GameId { get; set; }
        public int SquadId { get; set; }
        public int PlayerId { get; set; }
    }
}
