﻿namespace hvz_backend.Models.DTOs.Squad
{
    public class SquadEditDTO
    {
        public int SquadId { get; set; }
        public string Name { get; set; }
        public bool IsHuman { get; set; }
        public int GameId { get; set; }
    }
}
