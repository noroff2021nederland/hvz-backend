﻿namespace hvz_backend.Models.DTOs.Squad
{
    public class SquadCreateDTO
    {
        public string Name { get; set; }
        public int GameId { get; set; }
        public bool IsHuman { get; set; }
    }
}
