﻿namespace hvz_backend.Models.DTOs.Squad
{
    public class SquadReadDTO
    {
        public int SquadId { get; set; }
        public int GameId { get; set; }
        public string Name { get; set; }
        public bool IsHuman { get; set; }
    }
}
