﻿using System;

namespace hvz_backend.Models.DTOs.Game
{
    public class GameReadDTO
    {
        public int GameId { get; set; }
        public string Name { get; set; }
        public string GameState { get; set; }
        public double? NwLat { get; set; }
        public double? NwLon { get; set; }
        public double? SeLat { get; set; }
        public double? SeLon { get; set; }
        public string Description { get; set; }
        public DateTime DateOfCreation { get; set; }
        public DateTime DateOfCompletion { get; set; }
    }
}
