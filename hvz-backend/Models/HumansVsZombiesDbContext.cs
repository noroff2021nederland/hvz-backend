﻿using hvz_backend.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;

namespace hvz_backend.Models
{
    public class HumansVsZombiesDbContext : DbContext
    {
        public HumansVsZombiesDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<User> User { get; set; }
        public DbSet<Game> Game { get; set; }
        public DbSet<Player> Player { get; set; }
        public DbSet<Squad> Squad { get; set; }
        public DbSet<Mission> Mission { get; set; }
        public DbSet<Kill> Kill { get; set; }
        public DbSet<Chat> Chat { get; set; }
        public DbSet<SquadMember> SquadMember { get; set; }
        public DbSet<SquadCheckin> SquadCheckin { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Specify OnDelete behaviour for foreign key relationships
            modelBuilder.Entity<Kill>()
                .HasOne(k => k.Killer)
                .WithMany(p => p.Kills)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Kill>()
                .HasOne(k => k.Victim)
                .WithOne(p => p.Death)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Chat>()
                .HasOne(c => c.Player)
                .WithMany(p => p.Chats)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<SquadMember>()
                .HasOne(sm => sm.Player)
                .WithOne(p => p.MemberOfSquad)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<SquadMember>()
                .HasOne(sm => sm.Squad)
                .WithMany(s => s.Members)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<SquadCheckin>()
                .HasOne(sm => sm.Squad)
                .WithMany(s => s.Checkins)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<SquadCheckin>()
                .HasOne(sm => sm.SquadMember)
                .WithMany(s => s.Checkins)
                .OnDelete(DeleteBehavior.NoAction);

            // Seed game records
            var game1 = new Game
            {
                GameId = 1,
                GameState = "Completed",
                Name = "Game One",
                NwLat = 52.397994,
                NwLon = 4.808648,
                SeLat = 52.307994,
                SeLon = 4.898648,
                Description = "This game is easy"
            };

            var game2 = new Game
            {
                GameId = 2,
                GameState = "Registration",
                Name = "Game Two",
                NwLat = 52.395831,
                NwLon = 4.909162,
                SeLat = 52.305831,
                SeLon = 4.999162,
                Description = "This game is hard"
            };

            var game3 = new Game
            {
                GameId = 3,
                GameState = "In progress",
                Name = "Game Three",
                NwLat = 52.397994,
                NwLon = 4.808648,
                SeLat = 52.307994,
                SeLon = 4.898648,
                Description = "This game is scary"
            };

            modelBuilder.Entity<Game>()
                .HasData(game1, game2, game3);

            // Seed mission records
            var mission1 = new Mission
            {
                MissionId = 1,
                Name = "Mission One",
                IsHumanVisible = true,
                IsZombieVisible = false,
                Description = "Easy mission",
                StartTime = new DateTime(2021, 6, 20, 13, 1, 1),
                EndTime = new DateTime(2021, 6, 20, 14, 1, 1),
                GameId = 1,
                Lat = 52.357994,
                Lon = 4.868648
            };

            var mission2 = new Mission
            {
                MissionId = 2,
                Name = "Mission Two",
                IsHumanVisible = true,
                IsZombieVisible = false,
                Description = "Hard mission",
                StartTime = new DateTime(2021, 6, 20, 13, 1, 1),
                EndTime = new DateTime(2021, 6, 20, 14, 1, 1),
                GameId = 2,
                Lat = 52.357994,
                Lon = 4.868648
            };

            var mission3 = new Mission
            {
                MissionId = 3,
                Name = "Mission Three",
                IsHumanVisible = true,
                IsZombieVisible = false,
                Description = "Hard mission",
                StartTime = new DateTime(2021, 6, 20, 13, 1, 1),
                EndTime = new DateTime(2021, 6, 20, 14, 1, 1),
                GameId = 1,
                Lat = 52.350994,
                Lon = 4.865048
            };

            modelBuilder.Entity<Mission>()
                .HasData(mission1, mission2, mission3);

            // Seed user data
            var user1 = new User
            {
                UserId = 1,
                KeyCloakId = "2effbfa9-085f-4b54-aabe-49fbb04b939b",
                FirstName = "ZombieKiller",
                LastName = "Mike"
            };

            var user2 = new User
            {
                UserId = 2,
                KeyCloakId = "2effbfa9-085f-4b54-aabe-49fbb04b939e",
                FirstName = "Mike",
                LastName = "Ike"
            };

            var user3 = new User
            {
                UserId = 3,
                KeyCloakId = "2effbfa9-085f-4b54-aabe-49fbb04b939f",
                FirstName = "Elon",
                LastName = "Musk"
            };

            modelBuilder.Entity<User>()
                .HasData(user1, user2, user3);

            // Seed player data
            var player1 = new Player
            {
                PlayerId = 1,
                PlayerName = "Dewald",
                IsHuman = true,
                IsPatientZero = false,
                BiteCode = 1234,
                UserId = 1,
                GameId = 1
            };

            var player2 = new Player
            {
                PlayerId = 2,
                PlayerName = "Nicholas",
                IsHuman = false,
                IsPatientZero = false,
                BiteCode = 1231,
                UserId = 2,
                GameId = 1
            };

            var player3 = new Player
            {
                PlayerId = 3,
                PlayerName = "Anton",
                IsHuman = true,
                IsPatientZero = false,
                BiteCode = 1232,
                UserId = 3,
                GameId = 1
            };

            modelBuilder.Entity<Player>()
                .HasData(player1, player2, player3);

            // Seed squad data
            var squad1 = new Squad
            {
                SquadId = 1,
                Name = "Squad One",
                IsHuman = true,
                GameId = 1
            };

            var squad2 = new Squad
            {
                SquadId = 2,
                Name = "Squad Two",
                IsHuman = false,
                GameId = 1
            };

            modelBuilder.Entity<Squad>()
                .HasData(squad1, squad2);

            // Seed squadmember data
            var squadmember1 = new SquadMember
            {
                SquadMemberId = 1,
                Rank = "General",
                GameId = 1,
                SquadId = 1,
                PlayerId = 1
            };

            var squadmember2 = new SquadMember
            {
                SquadMemberId = 2,
                Rank = "Zombie General",
                GameId = 1,
                SquadId = 2,
                PlayerId = 2
            };

            modelBuilder.Entity<SquadMember>()
                .HasData(squadmember1, squadmember2);

            // Seed squadcheckin data
            var checkin1 = new SquadCheckin
            {
                SquadCheckinId = 1,
                StartTime = new DateTime(2021, 6, 21, 12, 1, 1),
                EndTime = new DateTime(2021, 6, 21, 13, 1, 1),
                Lat = 52.357994,
                Lon = 4.868648,
                GameId = 1,
                SquadId = 1,
                SquadMemberId = 1
            };

            var checkin2 = new SquadCheckin
            {
                SquadCheckinId = 2,
                StartTime = new DateTime(2021, 6, 20, 12, 1, 1),
                EndTime = new DateTime(2021, 6, 20, 13, 1, 1),
                Lat = 52.357994,
                Lon = 4.868648,
                GameId = 1,
                SquadId = 2,
                SquadMemberId = 2
            };

            modelBuilder.Entity<SquadCheckin>()
                .HasData(checkin1, checkin2);

            // Seed chat data
            var chat1 = new Chat
            {
                ChatId = 1,
                Message = "Hello!",
                IsHumanGlobal = true,
                IsZombieGlobal = false,
                Time = new DateTime(2021, 6, 20, 13, 1, 1),
                GameId = 1,
                PlayerId = 1,
                SquadId = 1
            };
            var chat2 = new Chat
            {
                ChatId = 2,
                Message = "Any one here?",
                IsHumanGlobal = true,
                IsZombieGlobal = false,
                Time = new DateTime(2021, 6, 20, 13, 1, 1),
                GameId = 1,
                PlayerId = 1,
                SquadId = 1
            };
            var chat3 = new Chat
            {
                ChatId = 3,
                Message = "To all the undead in the upper world",
                IsHumanGlobal = false,
                IsZombieGlobal = true,
                Time = new DateTime(2021, 6, 20, 13, 1, 1),
                GameId = 1,
                PlayerId = 2,
                SquadId = 2
            };
            var chat4 = new Chat
            {
                ChatId = 4,
                Message = "Fur everiwun",
                IsHumanGlobal = true,
                IsZombieGlobal = true,
                Time = new DateTime(2021, 6, 20, 13, 1, 1),
                GameId = 1,
                PlayerId = 2
            };
            var chat5 = new Chat
            {
                ChatId = 5,
                Message = "For Squad 1",
                IsHumanGlobal = false,
                IsZombieGlobal = false,
                Time = new DateTime(2021, 6, 20, 15, 1, 1),
                GameId = 1,
                PlayerId = 1,
                SquadId = 1
            };
            var chat6 = new Chat
            {
                ChatId = 6,
                Message = "For Squad 2",
                IsHumanGlobal = false,
                IsZombieGlobal = false,
                Time = new DateTime(2021, 6, 20, 16, 1, 1),
                GameId = 1,
                PlayerId = 2,
                SquadId = 2
            };

            modelBuilder.Entity<Chat>()
                .HasData(chat1, chat2, chat3, chat4, chat5, chat6);

            // Seed kill data
            var kill1 = new Kill
            {
                KillId = 1,
                TimeOfDeath = new DateTime(2021, 6, 20, 13, 1, 1),
                Story = "Elon's sock rocket exploded in his hands and got infected by Ike Mike",
                Lat = 52.357994,
                Lon = 4.868648,
                GameId = 1,
                KillerId = 2,
                VictimId = 3
            };

            modelBuilder.Entity<Kill>()
                .HasData(kill1);
        }
    }
}
