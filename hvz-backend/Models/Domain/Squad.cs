﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace hvz_backend.Models.Domain
{
    public class Squad
    {
        public int SquadId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public bool IsHuman { get; set; }
        public bool Deleted { get; set; }

        [Required]
        public int GameId { get; set; }
        public virtual Game Game { get; set; }
        public virtual ICollection<SquadMember> Members { get; set; }
        public virtual ICollection<SquadCheckin> Checkins { get; set; }

    }
}
