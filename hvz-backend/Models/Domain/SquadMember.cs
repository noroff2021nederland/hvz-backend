﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace hvz_backend.Models.Domain
{
    public class SquadMember
    {
        public int SquadMemberId { get; set; }
        [Required]
        [MaxLength(50)]
        public string Rank { get; set; }
        public bool Deleted { get; set; }

        [Required]
        public int GameId { get; set; }
        public virtual Game Game { get; set; }
        [Required]
        public int SquadId { get; set; }
        public virtual Squad Squad { get; set; }
        [Required]
        public int PlayerId { get; set; }
        public virtual Player Player { get; set; }
        public virtual ICollection<SquadCheckin> Checkins { get; set; }

    }
}
