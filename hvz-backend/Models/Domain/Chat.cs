﻿using System;
using System.ComponentModel.DataAnnotations;

namespace hvz_backend.Models.Domain
{
    public class Chat
    {
        public int ChatId { get; set; }
        [Required]
        public string Message { get; set; }
        [Required]
        public bool IsHumanGlobal { get; set; }
        [Required]
        public bool IsZombieGlobal { get; set; }
        [Required]
        public DateTime Time { get; set; }
        public bool Deleted { get; set; }

        [Required]
        public int GameId { get; set; }
        public virtual Game Game { get; set; }
        [Required]
        public int PlayerId { get; set; }
        public virtual Player Player { get; set; }
        public int? SquadId { get; set; }
        public virtual Squad Squad { get; set; }
    }
}
