﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace hvz_backend.Models.Domain
{
    public class SquadCheckin
    {
        public int SquadCheckinId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        [Column(TypeName = "decimal(9, 6)")]
        public double Lat { get; set; }
        [Column(TypeName = "decimal(9, 6)")]
        public double Lon { get; set; }
        public bool Deleted { get; set; }

        [Required]
        public int GameId { get; set; }
        public virtual Game Game { get; set; }
        [Required]
        public int SquadId { get; set; }
        public virtual Squad Squad { get; set; }
        [Required]
        public int SquadMemberId { get; set; }
        public virtual SquadMember SquadMember { get; set; }
    }
}
