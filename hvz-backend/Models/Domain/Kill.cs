﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace hvz_backend.Models.Domain
{
    public class Kill
    {
        public int KillId { get; set; }
        [Required]
        public DateTime TimeOfDeath { get; set; }
        public string Story { get; set; }
        [Column(TypeName = "decimal(9, 6)")]
        public double? Lat { get; set; }
        [Column(TypeName = "decimal(9, 6)")]
        public double? Lon { get; set; }
        public bool Deleted { get; set; }

        [Required]
        public int GameId { get; set; }
        public virtual Game Game { get; set; }
        public int KillerId { get; set; }
        public virtual Player Killer { get; set; }
        public int VictimId { get; set; }
        public virtual Player Victim { get; set; }
        public int BiteCode { get; set; }
    }
}
