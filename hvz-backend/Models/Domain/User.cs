﻿using System.ComponentModel.DataAnnotations;

namespace hvz_backend.Models.Domain
{
    public class User
    {
        public int UserId { get; set; }
        [Required]
        public string KeyCloakId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public bool Deleted { get; set; }
    }
}
