﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace hvz_backend.Models.Domain
{
    public class Player
    {
        public int PlayerId { get; set; }
        [Required]
        public string PlayerName { get; set; }
        [Required]
        public bool IsHuman { get; set; }
        [Required]
        public bool IsPatientZero { get; set; }
        [Required]
        public int BiteCode { get; set; }
        public bool Deleted { get; set; }

        [Required]
        public int UserId { get; set; }
        public User User { get; set; }
        [Required]
        public int GameId { get; set; }
        public virtual Game Game { get; set; }
        public virtual ICollection<Kill> Kills { get; set; }
        public virtual Kill Death { get; set; }
        public virtual ICollection<Chat> Chats { get; set; }
        public virtual SquadMember MemberOfSquad { get; set; }
    }
}
