﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace hvz_backend.Models.Domain
{
    public class Game
    {
        public int GameId { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        [MaxLength(50)]
        public string GameState { get; set; }
        [Column(TypeName = "decimal(9, 6)")]
        public double? NwLat { get; set; }
        [Column(TypeName = "decimal(9, 6)")]
        public double? NwLon { get; set; }
        [Column(TypeName = "decimal(9, 6)")]
        public double? SeLat { get; set; }
        [Column(TypeName = "decimal(9, 6)")]
        public double? SeLon { get; set; }
        [Required]
        public string Description { get; set; }
        public DateTime DateOfCreation { get; set; }
        public DateTime? DateOfCompletion { get; set; }
        public bool Deleted { get; set; }
    }
}
