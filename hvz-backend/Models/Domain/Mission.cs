﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace hvz_backend.Models.Domain
{
    public class Mission
    {
        public int MissionId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public bool IsHumanVisible { get; set; }
        [Required]
        public bool IsZombieVisible { get; set; }
        public string Description { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        [Column(TypeName = "decimal(9, 6)")]
        public double Lat { get; set; }
        [Column(TypeName = "decimal(9, 6)")]
        public double Lon { get; set; }
        public bool Deleted { get; set; }

        [Required]
        public int GameId { get; set; }
        public virtual Game Game { get; set; }
    }
}
