﻿using hvz_backend.Models;
using hvz_backend.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hvz_backend.Services
{
    public class UserService : IUserService
    {
        private readonly HumansVsZombiesDbContext _context;

        public UserService(HumansVsZombiesDbContext context)
        {
            _context = context;
        }

        public async Task<User> AddNewUserAsync(User user)
        {
            _context.User.Add(user);
            await _context.SaveChangesAsync();
            return user;          
        }

        public async Task<User> GetSpecificUserByKeyCloakIdAsync(string keyCloakId)
        {
            return await _context.User.Where(u => u.KeyCloakId == keyCloakId).FirstOrDefaultAsync();
        }
    }
}
