﻿using hvz_backend.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hvz_backend.Services
{
    public interface IUserService
    {
        public Task<User> GetSpecificUserByKeyCloakIdAsync(string keyCloakId);
        public Task<User> AddNewUserAsync(User domainUser);
    }
}
