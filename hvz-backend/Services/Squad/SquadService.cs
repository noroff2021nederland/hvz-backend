﻿using hvz_backend.Models;
using hvz_backend.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hvz_backend.Services
{
    public class SquadService : ISquadService
    {
        private readonly HumansVsZombiesDbContext _context;

        public SquadService(HumansVsZombiesDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Squad>> GetSquadsAsync(int id)
        {
            return await _context.Squad.Include(s => s.Game).Where(s => s.GameId == id).ToListAsync();
        }

        public async Task<Squad> GetSquadByIdAsync(int gameId, int squadId)
        {
            return await _context.Squad
                .Include(s => s.Game)
                .Where(s => s.GameId == gameId && s.SquadId == squadId)
                .FirstAsync();
        }

        public bool SquadExists(int gameId, int squadId)
        {
            return _context.Squad
                .Include(s => s.Game)
                .Any(s => s.GameId == gameId && s.SquadId == squadId);
        }

        public async Task<Squad> AddSquadAsync(Squad squad)
        {
            _context.Squad.Add(squad);
            await _context.SaveChangesAsync();
            return squad;
        }

        public async Task<SquadMember> JoinSquad(SquadMember member)
        {
            _context.SquadMember.Add(member);
            await _context.SaveChangesAsync();
            return member;
        }

        public async Task UpdateSquadAsync(Squad squad)
        {
            _context.Entry(squad).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteSquadAsync(int squadId)
        {
            Squad squad = await _context.Squad.FindAsync(squadId);
            _context.Squad.Remove(squad);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Chat>> GetChatMessagesAsync(int gameId, int squadId)
        {
            return await _context.Chat
                .Where(c => c.GameId == gameId && c.SquadId == squadId)
                .Where(c => (!c.IsHumanGlobal && !c.IsZombieGlobal))
                .ToListAsync();
        }

        public async Task<Chat> AddChatMessage(Chat chat)
        {
            _context.Chat.Add(chat);
            await _context.SaveChangesAsync();
            return chat;
        }

        public async Task<IEnumerable<SquadCheckin>> GetCheckinsAsync(int gameId, int squadId)
        {
            return await _context.SquadCheckin
                .Where(c => c.GameId == gameId && c.SquadId == squadId)
                .ToListAsync();
        }

        public async Task<SquadCheckin> AddCheckin(SquadCheckin squadCheckin)
        {
            _context.SquadCheckin.Add(squadCheckin);
            await _context.SaveChangesAsync();
            return squadCheckin;
        }

        public async Task<SquadMember> GetSquadMemberByPlayerIdAsync(int playerId)
        {
            return await _context.SquadMember
                .Where(s => s.PlayerId == playerId)
                .FirstOrDefaultAsync();
        }
    }
}
