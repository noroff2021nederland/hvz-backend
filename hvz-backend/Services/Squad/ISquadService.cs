﻿using hvz_backend.Models.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace hvz_backend.Services
{
    public interface ISquadService
    {
        public Task<IEnumerable<SquadCheckin>> GetCheckinsAsync(int gameId, int squadId);
        public Task<IEnumerable<Squad>> GetSquadsAsync(int id);
        public Task<IEnumerable<Chat>> GetChatMessagesAsync(int gameId, int squadId);
        public Task<SquadCheckin> AddCheckin(SquadCheckin squadCheckin);
        public Task<SquadMember> JoinSquad(SquadMember member);
        public Task<Squad> GetSquadByIdAsync(int gameId, int squadId);
        public Task<Squad> AddSquadAsync(Squad squad);
        public Task<Chat> AddChatMessage(Chat chat);
        public Task UpdateSquadAsync(Squad squad);
        public Task DeleteSquadAsync(int squadId);
        public bool SquadExists(int gameId, int squadId);
        Task<SquadMember> GetSquadMemberByPlayerIdAsync(int playerId);
    }
}
