﻿using hvz_backend.Models.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace hvz_backend.Services
{
    public interface IPlayerService
    {
        public Task<List<Player>> GetPlayersAsync(int id);
        public Task<Player> AddPlayerAsync(Player player);
        public Task<Player> GetSpecificPlayerAsync(int id);
        public Task<int> GetBiteCodeFromVictimAsync(int id);
        public Task<int> CountPlayersAsync(int id);
        public Task UpdatePlayerAsync(Player player);
        public Task DeletePlayerAsync(int id);
        public bool PlayerExistsInGame(int gameId, int playerId);
        public bool PlayerExists(int id);
        public bool UserExists(int id);
        public int GenerateBiteCode();
    }
}
