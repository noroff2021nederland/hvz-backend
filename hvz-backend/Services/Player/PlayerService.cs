﻿using hvz_backend.Models;
using hvz_backend.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hvz_backend.Services
{
    public class PlayerService : IPlayerService
    {
        private readonly HumansVsZombiesDbContext _context;
        private Random _random = new Random();

        public PlayerService(HumansVsZombiesDbContext context)
        {
            _context = context;
        }

        public async Task<List<Player>> GetPlayersAsync(int gameId)
        {
            return await _context.Player
                .Where(p => p.GameId == gameId)
                .ToListAsync();
        }

        public async Task<Player> GetSpecificPlayerAsync(int playerId)
        {
            return await _context.Player.FindAsync(playerId);
        }

        public bool PlayerExists(int id)
        {
            return _context.Player.Any(p => p.PlayerId == id);
        }

        public bool PlayerExistsInGame(int gameId, int playerId)
        {
            return _context.Player
                .Include(g => g.Game)
                .Any(p => p.GameId == gameId && p.PlayerId == playerId);
        }

        public async Task<Player> AddPlayerAsync(Player player)
        {
            player.IsHuman = true;
            player.IsPatientZero = false;
            player.BiteCode = GenerateBiteCode();

            _context.Player.Add(player);
            await _context.SaveChangesAsync();
            return player;
        }

        public bool UserExists(int Id)
        {
            return _context.User.Any(u => u.UserId == Id);
        }

        public async Task UpdatePlayerAsync(Player player)
        {
            _context.Entry(player).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task DeletePlayerAsync(int id)
        {
            Player player = await _context.Player.FindAsync(id);
            _context.Player.Remove(player);
            await _context.SaveChangesAsync();
        }

        public async Task<int> GetBiteCodeFromVictimAsync(int id)
        {
            Player p = await _context.Player
                .FindAsync(id);
            return p.BiteCode;
        }

        public async Task<int> CountPlayersAsync(int id)
        {
            return await _context.Player.Where(p => p.GameId == id).CountAsync();
        }

        public int GenerateBiteCode()
        {
            return _random.Next(10000, 99999);
        }
    }
}
