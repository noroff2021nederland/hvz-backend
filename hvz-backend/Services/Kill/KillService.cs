﻿using hvz_backend.Models;
using hvz_backend.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hvz_backend.Services
{
    public class KillService : IKillService
    {
        private readonly HumansVsZombiesDbContext _context;

        public KillService(HumansVsZombiesDbContext context)
        {
            _context = context;
        }

        public async Task<Kill> AddKillAsync(Kill kill)
        {
            _context.Kill.Add(kill);
            await _context.SaveChangesAsync();
            return kill;
        }

        public async Task<Kill> DeleteKillAsync(int id)
        {
            Kill kill = await _context.Kill.FindAsync(id);
            _context.Kill.Remove(kill);
            await _context.SaveChangesAsync();
            return kill;
        }

        public async Task<List<Kill>> GetKillsAsync(int id)
        {
            return await _context.Kill.Where(k => k.GameId == id)
                .ToListAsync();
        }

        public async Task<Kill> GetSpecificKillAsync(int id)
        {
            return await _context.Kill.FindAsync(id);
        }

        public bool KillExists(int id)
        {
            return _context.Kill.Any(k => k.KillId == id);
        }

        public async Task UpdateKillAsync(Kill kill)
        {
            _context.Entry(kill).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
