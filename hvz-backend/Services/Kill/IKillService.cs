﻿using hvz_backend.Models.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace hvz_backend.Services
{
    public interface IKillService
    {
        public Task<List<Kill>> GetKillsAsync(int id);
        public Task<Kill> GetSpecificKillAsync(int id);
        public Task<Kill> AddKillAsync(Kill kill);
        public Task<Kill> DeleteKillAsync(int id);
        public Task UpdateKillAsync(Kill kill);
        public bool KillExists(int id);
    }
}
