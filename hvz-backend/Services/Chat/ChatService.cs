﻿using hvz_backend.Models;
using hvz_backend.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hvz_backend.Services
{
    public class ChatService : IChatService
    {
        private readonly HumansVsZombiesDbContext _context;

        public ChatService(HumansVsZombiesDbContext context)
        {
            _context = context;
        }

        public async Task<Chat> AddChatMessageAsync(Chat chat)
        {
            _context.Chat.Add(chat);
            await _context.SaveChangesAsync();
            return chat;
        }

        public async Task<IEnumerable<Chat>> GetChatMessagesAsync(bool human, bool zombie)
        {
            return await _context.Chat
                .Where(c => (c.IsHumanGlobal && c.IsZombieGlobal) || c.IsHumanGlobal == human && c.IsZombieGlobal == zombie)
                .ToListAsync();
        }
    }
}
