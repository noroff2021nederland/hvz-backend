﻿using hvz_backend.Models.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace hvz_backend.Services
{
    public interface IChatService
    {
        public Task<Chat> AddChatMessageAsync(Chat chat);
        public Task<IEnumerable<Chat>> GetChatMessagesAsync(bool human, bool zombie);
    }
}
