﻿using hvz_backend.Models.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace hvz_backend.Services
{
    public interface IGameService
    {
        public Task<IEnumerable<Game>> GetGamesAsync();
        public Task<Game> GetSpecificGameAsync(int id);
        public Task<Game> AddNewGameAsync(Game game);
        public Task UpdateGameAsync(Game game);
        public Task DeleteGameAsync(int id);
        public bool GameExists(int id);
        public Task<bool> ValidLocationAsync(int id, double? lon, double? lat);
    }
}
