﻿using hvz_backend.Models;
using hvz_backend.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hvz_backend.Services
{
    public class GameService : IGameService
    {
        private readonly HumansVsZombiesDbContext _context;

        public GameService(HumansVsZombiesDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Game>> GetGamesAsync()
        {
            return await _context.Game.ToListAsync();
        }

        public async Task<Game> GetSpecificGameAsync(int id)
        {
            return await _context.Game.FindAsync(id);
        }

        public async Task<Game> AddNewGameAsync(Game game)
        {
            _context.Game.Add(game);
            await _context.SaveChangesAsync();
            return game;
        }

        public async Task UpdateGameAsync(Game game)
        {
            _context.Entry(game).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public bool GameExists(int id)
        {
            return _context.Game.Any(g => g.GameId == id);
        }

        public async Task DeleteGameAsync(int id)
        {
            Game game = await _context.Game.FindAsync(id);
            _context.Game.Remove(game);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> ValidLocationAsync(int id, double? lon, double? lat)
        {
            Game game = await _context.Game.FindAsync(id);

            if (lon <= game.NwLon || lon >= game.SeLon || lat >= game.NwLat || lat <= game.SeLat)
            {
                return false;
            }
            return true;
        }
    }
}
