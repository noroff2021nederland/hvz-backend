﻿using hvz_backend.Models.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace hvz_backend.Services
{
    public interface IMissionService
    {
        public Task<IEnumerable<Mission>> GetMissionsAsync(int id);
        public Task<Mission> GetMissionByIdAsync(int gameId, int missionId);
        public Task<Mission> AddMissionAsync(Mission mission);
        public Task<IEnumerable<Mission>> GetAllMissionsAsync();
        public Task UpdateMissionAsync(Mission mission);
        public Task DeleteMissionAsync(int missionId);
        public bool MissionExists(int gameId, int missionId);
    }
}
