﻿using hvz_backend.Models;
using hvz_backend.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hvz_backend.Services
{
    public class MissionService : IMissionService
    {
        private readonly HumansVsZombiesDbContext _context;

        public MissionService(HumansVsZombiesDbContext context)
        {
            _context = context;
        }

        public async Task<Mission> AddMissionAsync(Mission mission)
        {
            _context.Mission.Add(mission);
            await _context.SaveChangesAsync();
            return mission;
        }

        public async Task DeleteMissionAsync(int missionId)
        {
            Mission mission = await _context.Mission.FindAsync(missionId);
            _context.Mission.Remove(mission);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Mission>> GetAllMissionsAsync()
        {
            return await _context.Mission.ToListAsync();
        }

        public async Task<Mission> GetMissionByIdAsync(int gameId, int missionId)
        {
            return await _context.Mission
                .Include(m => m.Game)
                .Where(m => m.GameId == gameId && m.MissionId == missionId)
                .FirstAsync();
        }

        public async Task<IEnumerable<Mission>> GetMissionsAsync(int id)
        {
            return await _context.Mission.Include(s => s.Game).Where(s => s.GameId == id).ToListAsync();
        }

        public bool MissionExists(int gameId, int missionId)
        {
            return _context.Mission
                .Include(s => s.Game)
                .Any(s => s.GameId == gameId && s.MissionId == missionId);
        }

        public async Task UpdateMissionAsync(Mission mission)
        {
            _context.Entry(mission).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
