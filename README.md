# Humans vs Zombies Backend

## Setup
Add the file `appsettings.json` in the folder hvz-backend. Change the Data Source in the DefaultConnetion:
```
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "DefaultConnection": "Data Source=<ENTER_SOURCE>; Initial Catalog=HumansVsZombies; Integrated Security=True;"
  }
}
```

## Database

## API