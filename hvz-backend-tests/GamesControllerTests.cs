using AutoMapper;
using hvz_backend.Controllers;
using hvz_backend.Models.Domain;
using hvz_backend.Profiles;
using hvz_backend.Services;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace hvz_backend_tests
{
    public class GamesControllerTests
    {

        private List<Game> GetTestGames()
        {
            var games = new List<Game>
            {
                new Game()
                {
                    GameId = 99,
                    GameState = "open",
                    Name = "First Game"
                },
                new Game()
                {
                    GameId = 123,
                    GameState = "open",
                    Name = "Second Game"
                }
            };
            return games;
        }

        [Fact]
        public async Task GetGames_WhenCalledWithGetTestGames_ReturnsAListOfTwoGames()
        {
            // Arrange
            var mockRepo = new Mock<IGameService>();
            mockRepo.Setup(repo => repo.GetGamesAsync())
                .ReturnsAsync(GetTestGames());

            var profile = new GameProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(profile));
            var mockMapper = new Mapper(configuration);

            var controller = new GamesController(mockRepo.Object, mockMapper);

            // Act
            var result = await controller.GetGames();

            // Assert
            Assert.Equal(2, result.Value.Count());
        }

        [Fact]
        public async Task GetGames_WhenCalledWithGetTestGames_FirstGameShouldHaveId99()
        {
            // Arrange
            var mockRepo = new Mock<IGameService>();
            mockRepo.Setup(repo => repo.GetGamesAsync())
                .ReturnsAsync(GetTestGames());

            var profile = new GameProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(profile));
            var mockMapper = new Mapper(configuration);

            var controller = new GamesController(mockRepo.Object, mockMapper);

            // Act
            var result = await controller.GetGames();
            var firstGame = result.Value.First();

            // Assert
            Assert.Equal(99, firstGame.GameId);
        }

        private Game GetSpecificTestGame()
        {
            return new Game { GameId = 55, GameState = "Closed", Name = "Specific Game" };
        }

        [Fact]
        public async Task GetGame_WhenCalledWithAnId_ReturnSpecificGame()
        {
            // Arrange
            int id = 55;
            var mockRepo = new Mock<IGameService>();
            mockRepo.Setup(repo => repo.GetSpecificGameAsync(id))
                .ReturnsAsync(GetSpecificTestGame());

            var profile = new GameProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(profile));
            var mockMapper = new Mapper(configuration);

            var controller = new GamesController(mockRepo.Object, mockMapper);

            // Act
            var result = await controller.GetGame(id);
            var firstGame = result.Value;

            // Assert
            Assert.Equal(id, firstGame.GameId);
        }
    }
}
